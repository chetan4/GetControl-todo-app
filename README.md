GetControl todo app
============

The application has been developed as a part of assignment for GetControl. It contains login and dashboard pages. The dashboard contains todo list feature for the particular user.

## Layers

* UI Layer:
  * VueJS v2 using ``` vue-cli ```
  * VueJS modules: ``` vue-router, axios, bootstrap-vue ```
  * Bootstrap 4

* API Layer:
  * Followed JSONPlaceholder ``` https://jsonplaceholder.typicode.com ```
  * Used local JSON server ``` https://github.com/typicode/json-server ```
  * APIs: ``` users, todos ```

* Packaging Layer:
  * Webpack
  * NPM

* Unit testing:
  * Karma-mocha
  * Helper modules ``` sinon ``` and ``` bluebird ``` for mocking 'axios' based promise calls

## Prerequisites

  * Node should be installed on your machine
  * Run Local json server following steps as
    * Install json server ``` npm install -g json-server ```
    * Go the folder ``` data ``` and run command ``` json-server --watch db.json ```
      It will start server at ``` http://localhost:3000 ```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

### After application execution
  * Login using username ``` Mazahar ```
  * Perform the todo operations and view the results
  * Data get updated in ```  data/db.json ```
