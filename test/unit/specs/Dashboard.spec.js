import Dashboard from '@/components/Dashboard'

describe('Dashboard.vue', () => {
  it('should render correct contents', () => {
    expect(typeof Dashboard.data).to.equal('function')
    const defaultData = Dashboard.data()
    expect(defaultData.username).to.equal('')
    expect(defaultData.userId).to.equal('')
    expect(defaultData.todos.length).to.equal(0)
  })
  it('should fetch todos for given userid', () => {
    const data = () => ({ todos: [], username: 'Mazahar', userId: 1 })
    Dashboard.data = data
    expect(Dashboard.data().userId).to.equal(1)
  })
})
