import Login from '@/components/Login'

describe('Login.vue', () => {
  it('should render correct contents', () => {
    expect(typeof Login.data).to.equal('function')
    const defaultData = Login.data()
    expect(defaultData.username).to.equal('')
  })

  it('validated non-empty username', () => {
    const checkNonEmptyFun = Login.computed.state
    expect(typeof checkNonEmptyFun).to.equal('function')
    const isValid = checkNonEmptyFun.call({username: 'Test'})
    expect(isValid).to.equal('success')
  })
})
