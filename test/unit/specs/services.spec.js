import authService from '@/utils/auth-service'
import todosService from '@/utils/todos-service'
import sinon from 'sinon'
import Promise from 'bluebird'

import constants from '@/utils/constants'
let HTTP = constants.HTTP

let sandbox = null
beforeEach(() => { sandbox = sinon.sandbox.create() })
afterEach(() => { sandbox.restore() })

describe('auth-service.js', () => {
  it('should validate username', () => {
    let item = [{username: 'Mazahar'}]
    const resolved = new Promise((r) => r({ data: item }))
    sandbox.stub(HTTP, 'get').returns(resolved)
    
    // call login api
    authService.login(item.username)
      .then((d) => {
        expect(d.length).to.equal(1)
      })
      .catch((e) => {
        console.log('Error: ', e)
      })
  })
})

describe('todos-service.js', () => {
  // for todo list
  it('should return todos list for given user', () => {
    let todoList = [
      {
        'userId': 1,
        'id': 1,
        'title': 'Todo 1',
        'completed': true
      },
      {
        'title': 'Todo 3',
        'completed': false,
        'userId': 1,
        'id': 1
      }
    ]
    const resolved = new Promise((r) => r({ data: todoList }))
    sandbox.stub(HTTP, 'get').returns(resolved)
    
    // call get todos api
    todosService.createdTodos(todoList[0].userId)
      .then((d) => {
        expect(d.length).to.equal(2)
      })
      .catch((e) => {
        console.log('Error: ', e)
      })
  })

  // for create todo
  it('should create todo item', () => {
    let todoItem = {
      'userId': 1,
      'title': 'Todo New item',
      'completed': true
    }
    
    const resolved = new Promise((r) => r({ data: todoItem }))
    sandbox.stub(HTTP, 'post').returns(resolved)
    
    // call create todo api
    todosService.createTodo(todoItem)
      .then((d) => {
        expect(d).to.deep.equal({ 'userId': 1, 'title': 'Todo New item', 'completed': true })
      })
      .catch((e) => {
        console.log('Error: ', e)
      })
  })

  // for update todo
  it('should mark todo completed', () => {
    let todoItem = {
      'id': 2,
      'completed': true
    }
    
    const resolved = new Promise((r) => r({ data: todoItem }))
    sandbox.stub(HTTP, 'put').returns(resolved)
    
    // call update todo api
    todosService.updateTodo(todoItem)
      .then((d) => {
        expect(d).to.deep.equal({ 'id': 2, 'completed': true })
      })
      .catch((e) => {
        console.log('Error: ', e)
      })
  })
})
