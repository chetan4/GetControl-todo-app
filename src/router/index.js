/*
  * Name: router/index.js
  * Purpose: Create routing and include components
  * Developed by: Mazahar Shaikh <mazahar@idyllic.co>
  * Date : 20 Aug 2017
*/
import Vue from 'vue'
import Router from 'vue-router'

import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'bootstrap/dist/css/bootstrap.css'

// Components
import Login from '@/components/Login'
import Dashboard from '@/components/Dashboard'

// Authentication service
import Auth from '@/utils/auth-service.js'

Vue.use(Router)
Vue.use(BootstrapVue)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login,
      meta: { requiresAuth: false }
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: { requiresAuth: true }
    }
  ]
})

// for signed in session management
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!Auth.isSignedIn()) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    if (to.name === 'Login' && Auth.isSignedIn()) {
      next({
        path: '/dashboard'
      })
    } else {
      next()
    }
  }
})

export default router
