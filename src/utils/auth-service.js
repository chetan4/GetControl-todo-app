/*
  * Name: auth-service.js
  * Purpose: Authentication and client side session management
  * Developed by: Mazahar Shaikh <mazahar@idyllic.co>
  * Date : 20 Aug 2017
*/
import CONSTANTS from './constants'
let HTTP = CONSTANTS.HTTP
export default {
  userData () {
    let storedUser = JSON.parse(localStorage.getItem('user'))
    let user = {}
    if (storedUser) {
      user.username = storedUser[0].name
      user.userId = storedUser[0].id
    }
    return user
  },
  isSignedIn () {
    let storedUser = localStorage.getItem('user')
    let flag = false
    if (storedUser) {
      flag = true
    }
    return flag
  },
  login (username) {
    return (HTTP.get('users?username=' + username)
    .then(function (response) {
      let data = response.data
      if (data.length) {
        localStorage.setItem('user', JSON.stringify(data))
      }
      return data
    })
    .catch(e => e))
  },
  logout () {
    localStorage.removeItem('user')
  }
}
