/*
  * Name: constants.js
  * Purpose: Holds the constant messages and values.
  * It also contains common 'axios' for HTTP calls.
  * Developed by: Mazahar Shaikh <mazahar@idyllic.co>
  * Date : 21 Aug 2017
*/
import axios from 'axios'
export default {
  HTTP: axios.create({
    baseURL: 'http://ec2-54-234-123-114.compute-1.amazonaws.com:3000/',
    headers: {
      Authorization: 'Bearer {token}'
    }
  }),
  VALIDATION_MSG: {
    INVALID_USERNAME: { msg: 'Please enter valid username', type: 'danger' },
    ADDED_TODO: { msg: 'Todo is Created', type: 'success' },
    UPDATED_TODO: { msg: 'Todo is Updated', type: 'success' }
  }
}
