/*
  * Name: todos-service.js
  * Purpose: Todo list api calls
  * Developed by: Mazahar Shaikh <mazahar@idyllic.co>
  * Date : 20 Aug 2017
*/
import CONSTANTS from './constants'
let HTTP = CONSTANTS.HTTP
export default {
  createdTodos (userId) {
    return (HTTP.get('todos', {
      params: { userId: userId }
    })
    .then(response => response.data)
    .catch(e => e))
  },
  createTodo (postBody) {
    return (HTTP.post('todos', postBody)
    .then(response => response.data)
    .catch(e => e))
  },
  updateTodo (postBody) {
    return (HTTP.put('todos/' + postBody.id, postBody)
    .then(response => response.data)
    .catch(e => e))
  }
}
